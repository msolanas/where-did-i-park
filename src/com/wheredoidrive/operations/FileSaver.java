package com.wheredoidrive.operations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class FileSaver {

	public boolean savePosition(String data, Context c){
		String filename = "positions.txt";
		
		File sdCard = Environment.getExternalStorageDirectory(); 
		File dir = new File (sdCard.getAbsolutePath() + "/whereDidIPark/"); 
		dir.mkdirs(); 
	    FileOutputStream fos;       
	    String path = sdCard.getAbsolutePath() + "/whereDidIPark/"+filename;
	    Long tsLong = System.currentTimeMillis()/1000;
	    String ts = tsLong.toString();
	    data = data.replace("[", "");
	    data = data.replace("]", "");
	    data = data.replace(" ","");
	    data = data+","+ts+"\n";
	    Log.i("WHERE","Saving to file: "+data);
	    Log.i("WHERE",c.getApplicationContext().getFilesDir().getAbsolutePath());
	
		    
		    try {
		    	fos = new FileOutputStream(path,true);
		    	
	 
				// get the content in bytes
				byte[] contentInBytes = data.getBytes();
				fos.write(contentInBytes);
				fos.flush();
			     fos.close();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
	     return true;
	}
}
