package com.wheredoidrive.operations;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ohmage.mobility.SummaryMapActivity;

import android.content.Context;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.model.LatLng;

public class ApiClient {
	
	private AQuery aq;
	Context context;
	SummaryMapActivity av;
	
	public ApiClient(SummaryMapActivity s){
		context = s.getApplicationContext();
		av = s;
		aq = new AQuery(context);
	}
	
	public void getParkingsJson(){
	        
		
	        //perform a Google search in just a few lines of code
	        
	        String url = "http://env-4126955.jelastic.servint.net/DSProductCatalog/api/productOffering?productCategories.name=parking-lot";
	        
	        aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
	
	                @Override
	                public void callback(String url, JSONObject json, AjaxStatus status) {
	                        
	                	
	                        if(json != null){
	                                
	                                //successful ajax call, show status code and json content
	                        	// loop array
	                    		
	                    		
	                    			
	                    			
									try {
										ArrayList<String> list = new ArrayList<String>();
										JSONArray msg = (JSONArray) json.get("description");
										for (int i=0;i<msg.length();i++)
			                    		{
											String a;
										
										list.add(msg.getString(i));
										a = msg.getString(i);
										String[] b = a.split("|");
		                    			if (b.length > 1)
		                    				av.addMarker(new LatLng(Double.parseDouble(b[0]),Double.parseDouble(b[1])), Double.parseDouble("1"), false, null, null);
			                    		}
			                    	} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                    			
	                    		
	                    		
	                                //Toast.makeText(aq.getContext(), status.getCode() + ":" + json.toString(), Toast.LENGTH_LONG).show();
	                        
	                        }else{
	                                
	                                //ajax error, show error code
	                                //Toast.makeText(aq.getContext(), "Error downloading parking list:" + status.getCode(), Toast.LENGTH_LONG).show();
	                        }
	                }
	        });
	        
	}

}
