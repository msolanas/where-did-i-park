package com.wheredoidrive.operations;

public abstract class Operation {

	public abstract boolean execute();
}
