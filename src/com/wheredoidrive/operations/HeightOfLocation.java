package com.wheredoidrive.operations;

import android.app.Service;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class HeightOfLocation implements SensorEventListener{

	private SensorManager sensorManager;
	double sprtZ;
	double gZero = 9.80665; 
	int earthR = 6378137;
	public static float[] barometer = new float[10];
	
	public HeightOfLocation(Context context){
		sensorManager=(SensorManager)context.getSystemService(Service.SENSOR_SERVICE);
		
		// add listener. The listener will be HelloAndroid (this) class
		sensorManager.registerListener(this, 
				sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE),
				SensorManager.SENSOR_DELAY_NORMAL);
            // this was being executed but didn't really do anything
       
	}
	
	public float getPressure(){
		Log.i("WHERE","HEY"+String.valueOf(barometer[0]));
		return barometer[0];

	}
	
	public float getHeight(float prePressure, float nowPressure){
		float preHeight = (float)(153.8 * (15 + 273.2) * (1-Math.pow((double)prePressure / (double)1017, (double)0.1902)));
		float nowHeight = (float)(153.8 * (15 + 273.2) * (1-Math.pow((double)nowPressure / (double)1017, (double)0.1902)));
		return nowHeight - preHeight;
		
	} 
	
	public void onSensorChanged(SensorEvent event){
		if (event.sensor.getType() == Sensor.TYPE_PRESSURE)
		{
			// Get a local copy of the sensor values
			System.arraycopy(event.values, 0, barometer, 0,
					event.values.length);
			
		}
		sensorManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
}
