package org.ohmage.mobility;

import java.util.ArrayList;
import java.util.Vector;

import org.ohmage.logprobe.Log;

import android.content.Context;

import com.wheredoidrive.operations.FileSaver;
import com.wheredoidrive.operations.GPSTracker;
import com.wheredoidrive.operations.HeightOfLocation;

public class ParkingActionDetector {
	//Status will be 0=still
	//1=walking
	//2=driving
	//3=other
	private static int status=3;
	private static double prob = 1.0;
	private static boolean haveLoc = false;
	private static int minDrivingCicles = 4;
	private static int minWalkingCicles = 2;
	private static int minStillCicles = 1;
	private static boolean driving = false;
	GPSTracker gps;
	private ArrayList<Vector> positions;
	private Context context;
	
	public ParkingActionDetector(Context c){
		context = c;
		setPositions(new ArrayList<Vector>());
		Log.i("WHERE","Created Detector");
	}
	
	public boolean walk(){
		prob -= 0.2;
		switch (status)
		{
		case 0:
		case 2://driving
			
			
			if (prob <= 0.0 && driving)
			{
				getAndStoreLocation();
				setHasLoc(true);
				Log.i("WHERE","Walking after driving Prob: "+String.valueOf(prob)+" driving: "+String.valueOf(driving));
				driving = false;
			}
			else
			{
				Log.i("WHERE","Still still/driving. Prob: "+String.valueOf(prob)+" driving: "+String.valueOf(driving));
				if (prob>0.0)
					return true;
			}
				
		}
		Log.i("WHERE","WALKING/RUNNING Prob: "+String.valueOf(prob)+" driving: "+String.valueOf(driving));
		if (prob<=0.0 || status == 1)
		{
			prob = 1.0;
			status = 1;
		}
		return true;
	}
	
	public boolean still(){
		/*prob -= 0.2;
		
		Log.i("WHERE","Still but not updating, at least yet. Prob: "+String.valueOf(prob)+" driving: "+String.valueOf(driving));
		setHasLoc(false);
		if (prob<=0.0 || status == 0){
			Log.i("WHERE","STILL");
			prob = 1.0;
			status = 0;
		}*/
	
		return true;
	}
	
	public boolean drive(){
		//Perform some actions?
		prob -= 0.2;
		if (prob <= 0.0 || status == 2)
		{
			prob = 1.0;
			status = 2;
			Log.i("WHERE","Driving. Prob: "+String.valueOf(prob)+" driving: "+String.valueOf(driving));
			setHasLoc(false);
			driving = true;
		}
		return true;
	}
	
	public boolean other(){
		prob -= 0.2;
		if (prob <= 0.0 || status == 3)
		{
			prob = 1.0;
			status = 3;
			Log.i("WHERE","Other");
			setHasLoc(false);
		}
		
		return true;
	}
	
	private void getAndStoreLocation() {
		// TODO Auto-generated method stub
		gps = new GPSTracker(context);
		HeightOfLocation h = new HeightOfLocation(context);
		Log.i("WHERE","Getting Location");
		// check if GPS enabled		
        if(gps.canGetLocation()){
        	
        	double latitude = gps.getLatitude();
        	double longitude = gps.getLongitude();
        	float accuracy = gps.getAccuracy();
        	Vector a = new Vector();
        	a.add(latitude);
        	a.add(longitude);
        	a.add(accuracy);
        	a.add(h.getPressure());
        	positions.add(a);
        	FileSaver fs = new FileSaver();
        	
        	fs.savePosition(a.toString(), context);

        }else{
        	// can't get location
        	// GPS or Network is not enabled
        	// Ask user to enable GPS/network in settings
        	gps.showSettingsAlert();
        }
	}

	public boolean hasLoc() {
		return haveLoc;
	}

	public void setHasLoc(boolean haveLoc) {
		this.haveLoc = haveLoc;
	}

	public ArrayList<Vector> getPositions() {
		return positions;
	}

	public void setPositions(ArrayList<Vector> positions) {
		this.positions = positions;
	}


}
