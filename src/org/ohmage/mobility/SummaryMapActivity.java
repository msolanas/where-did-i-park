/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohmage.mobility;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import android.R.color;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccount;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wheredoidrive.operations.ApiClient;
import com.wheredoidrive.operations.Dropbox;
import com.wheredoidrive.operations.HeightOfLocation;

/**
 * This the app's main Activity. It provides buttons for requesting the various features of the
 * app, displays the current location, the current address, and the status of the location client
 * and updating services.
 *
 * {@link #getLocation} gets the current location using the Location Services getLastLocation()
 * function. {@link #getAddress} calls geocoding to get a street address for the current location.
 * {@link #startUpdates} sends a request to Location Services to send periodic location updates to
 * the Activity.
 * {@link #stopUpdates} cancels previous periodic update requests.
 *
 * The update interval is hard-coded to be 5 seconds.
 */
public class SummaryMapActivity extends Activity implements
        LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {
	private MapFragment mf;
	GoogleMap gm;
    // A request to connect to Location Services
    private LocationRequest mLocationRequest;
    private ApiClient api;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    boolean running = false;
	SharedPreferences settings;
	public static final String MOBILITY_ON = "mobility_on";
    // Handles to UI widgets
	static final int REQUEST_LINK_TO_DBX = 1;

	public boolean dropboxSync = false;

	private DbxAccountManager mDbxAcctMgr;

	private DbxAccount mAccount;

	private DbxDatastore store;
    
   
    

    // Handle to SharedPreferences for this app
    SharedPreferences mPrefs;

    // Handle to a SharedPreferences editor
    SharedPreferences.Editor mEditor;

    /*
     * Note if updates have been turned on. Starts out as "false"; is set to "true" in the
     * method handleRequestSuccess of LocationUpdateReceiver.
     *
     */
    boolean mUpdatesRequested = false;

    /*
     * Initialize the Activity
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary_map);
        settings = getSharedPreferences(Mobility.MOBILITY, Context.MODE_PRIVATE);
        // Get handles to the UI view objects
        
        mf = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
       gm = mf.getMap();
       gm.setMyLocationEnabled(true);
        
        // Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Note that location updates are off until the user turns them on
        mUpdatesRequested = true;

        // Open Shared Preferences
        mPrefs = getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get an editor
        mEditor = mPrefs.edit();

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        mLocationClient = new LocationClient(this, this, this);
        
        
        

    }

    
   
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.control_menu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_show_settings:
	            startActivity(new Intent(this, MobilityControl.class));
	            return true;
	        case R.id.menu_connect_dropbox:
	        	dropboxSync = true;
	            connectToDropbox();
	        	 //startActivity(new Intent(this, Dropbox.class));
	            return true;
	        
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
    
    /*
     * Called when the Activity is no longer visible at all.
     * Stop updates and disconnect.
     */
    @Override
    public void onStop() {

        // If the client is connected
        if (mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // After disconnect() is called, the client is considered "dead".
        mLocationClient.disconnect();

        super.onStop();
    }
    /*
     * Called when the Activity is going into the background.
     * Parts of the UI may be visible, but the Activity is inactive.
     */
    @Override
    public void onPause() {

        // Save the current setting for updates
        mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, mUpdatesRequested);
        mEditor.commit();

        super.onPause();
    }

    /*
     * Called when the Activity is restarted, even before it becomes visible.
     */
    @Override
    public void onStart() {

        super.onStart();

        /*
         * Connect the client. Don't re-start any requests here;
         * instead, wait for onResume()
         */
        mLocationClient.connect();
        

    }
    /*
     * Called when the system detects that this Activity is now visible.
     */
    @Override
    public void onResume() {
        super.onResume();

        // If the app already has a setting for getting location updates, get it
        if (mPrefs.contains(LocationUtils.KEY_UPDATES_REQUESTED)) {
            mUpdatesRequested = mPrefs.getBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);

        // Otherwise, turn off location updates until requested
        } else {
            mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);
            mEditor.commit();
        }
        enableServiceDialog();
        
    }

    public boolean enableServiceDialog(){
		settings = getSharedPreferences(Mobility.MOBILITY, Context.MODE_PRIVATE);
        running = settings.getBoolean(MOBILITY_ON, false);
        if (!running)
        {
        	new AlertDialog.Builder(this)
            .setTitle("Where did I park is not running!")
            .setMessage("In order to use this app you need to able Where did I park's service.")
            .setPositiveButton("Enable it", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) { 
                    //We start the service
                	SharedPreferences settings = SummaryMapActivity.this.getSharedPreferences(Mobility.MOBILITY, Context.MODE_PRIVATE);
					Editor editor = settings.edit();
					editor.putBoolean(MOBILITY_ON, true);
					editor.commit();
//					startService(new Intent(MobilityControl.this, Mobility.class));
					Mobility.start(SummaryMapActivity.this.getApplicationContext());
                }
             })
            .setNegativeButton("Exit app", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) { 
                	finish();
                }
             })
             .show();
        }
        return true;
	}
    
    /*
     * Handle results returned to this Activity by other Activities started with
     * startActivityForResult(). In particular, the method onConnectionFailed() in
     * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
     * start an Activity that handles Google Play services problems. The result of this
     * call returns here, to onActivityResult.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        // Choose what to do based on the request code
        switch (requestCode) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:

                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.resolved));

                        // Display the result
                        
                    break;

                    // If any other result was returned by Google Play services
                    default:
                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.no_resolution));

                        // Display the result
                        
                    break;
                }

            // If any other request code was received
            default:
               // Report that this Activity received an unknown requestCode
               Log.d(LocationUtils.APPTAG,
                       getString(R.string.unknown_activity_request_code, requestCode));

               break;
        }
        if (requestCode == REQUEST_LINK_TO_DBX) {

			if (resultCode == Activity.RESULT_OK) {

				mAccount = mDbxAcctMgr.getLinkedAccount();

				// ... Now display your own UI using the linked account
				// information.

			} else {

				// ... Link failed or was cancelled by the user.

			}

		} else {

			super.onActivityResult(requestCode, resultCode, intent);

		}
    }

    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d(LocationUtils.APPTAG, getString(R.string.play_services_available));

            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0);
            if (dialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                //errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
            }
            return false;
        }
    }

    /**
     * Invoked by the "Get Location" button.
     *
     * Calls getLastLocation() to get the current location
     *
     * @param v The view object associated with this method, in this case a Button.
     */
    public void getLocation(View v) {

        // If Google Play Services is available
        if (servicesConnected()) {

            // Get the current location
            Location currentLocation = mLocationClient.getLastLocation();

            // Display the current location in the UI
           
            
         // Creating a LatLng object for the current location
            LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
     
            // Showing the current location in Google Map
            gm.moveCamera(CameraUpdateFactory.newLatLng(latLng));
     
            // Zoom in the Google Map
            gm.animateCamera(CameraUpdateFactory.zoomTo(15));
           
            
        }
    }

    public void addMarker(LatLng latLng,Double acc, boolean last, Long timestamp, Float initialPressure)
    {
    	settings = getSharedPreferences(Mobility.MOBILITY, Context.MODE_PRIVATE);
    	HeightOfLocation h = new HeightOfLocation(this.getApplicationContext());
    	Float height = (float) 0.0;
    	if (initialPressure != null)
    	{
    		Log.i("WHERE",String.valueOf(initialPressure));
    		Float newPressure = h.getPressure();
    		Log.i("WHERE",String.valueOf(newPressure));
	    	height = h.getHeight(initialPressure, newPressure);
	    	Log.i("WHERE",String.valueOf(height));
    	}
	    	
    	MarkerOptions mo;
    	if (timestamp!=null)
    	{
    		Timestamp stamp = new Timestamp(timestamp);
      	  Date date = new Date(stamp.getTime());
      	String title = "You parked here at "+date.toString()+". Alt. "+(initialPressure != null ? String.valueOf((int)Math.floor((height)))+"m (approx."+String.valueOf((int)Math.floor((height/3)))+ "floors)" : ".");
      	mo = new MarkerOptions().position(latLng).title(title);
    	}
    	else
    	{
    		mo = new MarkerOptions().position(latLng).title("You parked here");
    	}
    	  
    	
    	
    	boolean history= settings.getBoolean("HISTORY", false);
		if (last || history){
	    	gm.addMarker(mo);
	        CircleOptions co = new CircleOptions().center(latLng).radius(acc/2.0).fillColor(Color.argb(50, 0, 0, 255)).strokeWidth(1).strokeColor(Color.argb(80, 0, 0, 255));
	        gm.addCircle(co);
		}
    }
    
   
    
    public void readPositions(){
    	String filename = "positions.txt";
		File sdCard = Environment.getExternalStorageDirectory(); 
		File dir = new File (sdCard.getAbsolutePath() + "/whereDidIPark/"); 
		dir.mkdirs(); 
	    FileInputStream fos;       
	    File file = new File(sdCard.getAbsolutePath() + "/whereDidIPark/"+filename);
	    
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;
			while ((line = br.readLine()) != null) {
			   String[] s = line.split(",");
			   Vector v = new Vector();
			   v.addAll(Arrays.asList(s));
			   LatLng latLng = new LatLng(Double.parseDouble(s[0]),Double.parseDouble(s[1]));
			   addMarker(latLng,Double.parseDouble(s[2]),br.ready() ? false : true, s.length>3 ? Long.parseLong(s[4]) : null,s.length>4 ? Float.parseFloat(s[3]) : null);
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
    }
    /**
     * Invoked by the "Get Address" button.
     * Get the address of the current location, using reverse geocoding. This only works if
     * a geocoding service is available.
     *
     * @param v The view object associated with this method, in this case a Button.
     */
    // For Eclipse with ADT, suppress warnings about Geocoder.isPresent()
    @SuppressLint("NewApi")
    public void getAddress(View v) {

        // In Gingerbread and later, use Geocoder.isPresent() to see if a geocoder is available.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && !Geocoder.isPresent()) {
            // No geocoder is present. Issue an error message
            Toast.makeText(this, R.string.no_geocoder_available, Toast.LENGTH_LONG).show();
            return;
        }

        if (servicesConnected()) {

            // Get the current location
            Location currentLocation = mLocationClient.getLastLocation();

            // Turn the indefinite activity indicator on
           

            // Start the background task
            (new SummaryMapActivity.GetAddressTask(this)).execute(currentLocation);
        }
    }

    /**
     * Invoked by the "Start Updates" button
     * Sends a request to start location updates
     *
     * @param v The view object associated with this method, in this case a Button.
     */
    public void startUpdates(View v) {
        mUpdatesRequested = true;

        if (servicesConnected()) {
            startPeriodicUpdates();
        }
    }

    /**
     * Invoked by the "Stop Updates" button
     * Sends a request to remove location updates
     * request them.
     *
     * @param v The view object associated with this method, in this case a Button.
     */
    public void stopUpdates(View v) {
        mUpdatesRequested = false;

        if (servicesConnected()) {
            stopPeriodicUpdates();
        }
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        //mConnectionStatus.setText(R.string.connected);

    	startUpdates(null);
        getLocation(null);
        readPositions();
     // Instantiates a new CircleOptions object and defines the center and
        // radius
        api = new ApiClient(this);
        api.getParkingsJson();
        addParking();
    }

    private void addParking() {
		// TODO Auto-generated method stub
    	MarkerOptions mo;
    	mo = new MarkerOptions().position(new LatLng(37.77355400965652,-122.41617769002914)).title("Parking @ Parisoma (100 free spaces");
    	gm.addMarker(mo);
    	mo = new MarkerOptions().position(new LatLng(37.77519246716848,-122.41395950317383)).title("Parking Random (100 free spaces");
    	gm.addMarker(mo);
	}



	/*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        
    }
    
    public void connectToDropbox(){
    	if (dropboxSync == true) {

			mDbxAcctMgr = DbxAccountManager.getInstance(
					getApplicationContext(), "1levah7g8yjfegr",
					"jyrgcv3eqw8c8nt");

			mDbxAcctMgr.startLink((Activity) SummaryMapActivity.this,
					REQUEST_LINK_TO_DBX);

			if (mDbxAcctMgr.hasLinkedAccount()) {

				mAccount = mDbxAcctMgr.getLinkedAccount();

			}

			else {

			}

			try {

				dropboxSync();

			} catch (DbxException e) {

				// TODO Auto-generated catch block

				e.printStackTrace();

			}

		}

    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /**
     * Report location updates to the UI.
     *
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {

        // Report to the UI that the location was updated
       

        // In the UI, set the latitude and longitude to the value received
        
    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
       
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
        
    }

    /**
     * An AsyncTask that calls getFromLocation() in the background.
     * The class uses the following generic types:
     * Location - A {@link android.location.Location} object containing the current location,
     *            passed as the input parameter to doInBackground()
     * Void     - indicates that progress units are not used by this subclass
     * String   - An address passed to onPostExecute()
     */
    protected class GetAddressTask extends AsyncTask<Location, Void, String> {

        // Store the context passed to the AsyncTask when the system instantiates it.
        Context localContext;

        // Constructor called by the system to instantiate the task
        public GetAddressTask(Context context) {

            // Required by the semantics of AsyncTask
            super();

            // Set a Context for the background task
            localContext = context;
        }

        /**
         * Get a geocoding service instance, pass latitude and longitude to it, format the returned
         * address, and return the address to the UI thread.
         */
        @Override
        protected String doInBackground(Location... params) {
            /*
             * Get a new geocoding service instance, set for localized addresses. This example uses
             * android.location.Geocoder, but other geocoders that conform to address standards
             * can also be used.
             */
            Geocoder geocoder = new Geocoder(localContext, Locale.getDefault());

            // Get the current location from the input parameter list
            Location location = params[0];

            // Create a list to contain the result address
            List <Address> addresses = null;

            // Try to get an address for the current location. Catch IO or network problems.
            try {

                /*
                 * Call the synchronous getFromLocation() method with the latitude and
                 * longitude of the current location. Return at most 1 address.
                 */
                addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1
                );

                // Catch network or other I/O problems.
                } catch (IOException exception1) {

                    // Log an error and return an error message
                    Log.e(LocationUtils.APPTAG, getString(R.string.IO_Exception_getFromLocation));

                    // print the stack trace
                    exception1.printStackTrace();

                    // Return an error message
                    return (getString(R.string.IO_Exception_getFromLocation));

                // Catch incorrect latitude or longitude values
                } catch (IllegalArgumentException exception2) {

                    // Construct a message containing the invalid arguments
                    String errorString = getString(
                            R.string.illegal_argument_exception,
                            location.getLatitude(),
                            location.getLongitude()
                    );
                    // Log the error and print the stack trace
                    Log.e(LocationUtils.APPTAG, errorString);
                    exception2.printStackTrace();

                    //
                    return errorString;
                }
                // If the reverse geocode returned an address
                if (addresses != null && addresses.size() > 0) {

                    // Get the first address
                    Address address = addresses.get(0);

                    // Format the first line of address
                    String addressText = getString(R.string.address_output_string,

                            // If there's a street address, add it
                            address.getMaxAddressLineIndex() > 0 ?
                                    address.getAddressLine(0) : "",

                            // Locality is usually a city
                            address.getLocality(),

                            // The country of the address
                            address.getCountryName()
                    );

                    // Return the text
                    return addressText;

                // If there aren't any addresses, post a message
                } else {
                  return getString(R.string.no_address_found);
                }
        }

        /**
         * A method that's called once doInBackground() completes. Set the text of the
         * UI element that displays the address. This method runs on the UI thread.
         */
        @Override
        protected void onPostExecute(String address) {

            // Turn off the progress bar
           
        }
    }

    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this,
            LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            //errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }

    /**
     * Define a DialogFragment to display the error dialog generated in
     * showErrorDialog.
     */
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        /**
         * Default constructor. Sets the dialog field to null
         */
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        /**
         * Set the dialog to display
         *
         * @param dialog An error dialog
         */
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        /*
         * This method must return a Dialog to the DialogFragment.
         */
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    
    /**
	 * 
	 * Syncs the user details into the Dropbox cloud
	 * 
	 * @throws DbxException
	 */

	public void dropboxSync() throws DbxException {
		mAccount = mDbxAcctMgr.getLinkedAccount();
		DbxDatastore store = DbxDatastore.openDefault(mAccount);

		DbxTable settingsTbl = store.getTable("settings");

		DbxRecord SettingsTable = settingsTbl.insert().set("userid", 1)

		.set("license plate", "XB1XH7G").set("forename", "Stanley")

		.set("surname", "Kubrick").set("second interval", 5);

		store.sync();

		String plate = SettingsTable.getString("license plate");

		System.out.println(plate);

	}
}
