Where did I park?
=============

This is a fork of MobilityPhone, an activity classification service for android. The templates are provided by ohmage server.

Integration
===========

- tmforum's Catalog API to keep find the free car park slots around you
- Drobox SDK to sign in and sync the data (for later sharing it)


Dependencies
============

* [ohmageProbeLibrary](https://github.com/cens/ohmageProbeLibrary)
* [AccelService](https://github.com/ohmage/AccelService)
* [WiFiGPSLocation](https://github.com/ohmage/WiFiGPSLocation)
* [LogProbe](https://github.com/cens/LogProbe)